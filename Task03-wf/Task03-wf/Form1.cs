﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Task03_wf
{
    public partial class Form1 : Form
    {
        static string q1 = "Press a button for a list of names";
        static string f1 = "apple";
        static string f2 = "peach";
        static string f3 = "pear";
        static string f4 = "grapes";
        static string c1 = "blue";
        static string c2 = "pink";
        static string c3 = "orange";
        static string c4 = "yellow";
        static string n1 = "mr mittens";
        static string n2 = "wiskers";
        static string n3 = "Captian meow";
        static string n4 = "frank";        
        public Form1()
        {
            InitializeComponent();
            label1.Text=(q1);
        }

        private void button1_Click(object sender, EventArgs e)
        {
            label2.Text =(f1);
            label3.Text = (f2);
            label4.Text = (f3);
            label5.Text = (f4);
        }

        private void button2_Click(object sender, EventArgs e)
        {
            
            label2.Text = (n1);
            label3.Text = (n2);
            label4.Text = (n3);
            label5.Text = (n4);
        }

        private void button3_Click(object sender, EventArgs e)
        {
            
            label2.Text = (c1);
            label3.Text = (c2);
            label4.Text = (c3);
            label5.Text = (c4);
        }
    }
}
