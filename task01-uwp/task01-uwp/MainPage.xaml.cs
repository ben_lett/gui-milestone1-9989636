﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=402352&clcid=0x409

namespace task01_uwp
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class MainPage : Page
    {
        public static string q1 = "What would you like to convert to?";
        public static string q2 = "\n1. KMs to Miles";
        public static string q3 = "\n2. Miles to Kms ";
        public static string q4 = "\n3. exit the program";
        public static string q5 = "Enter the ammount of kms you want to conver to miles";
        public static string q6 = "Enter the ammount of Miles you want to conver to KM's";
        public static string q7 = "Enter the ammount of miles or kms you want to convert";
        public static string enter = "press<enter> to continue";
        public static string thanks = "Thanks for using";
        public static string please = "Please choose a number listed.";
        public static string convert = "your converted distance is";
        public static string k = "kms";
        public static string m = "miles";
        double km, km2, miles2;

        private void button1_Click(object sender, RoutedEventArgs e)
        {
            
            km = int.Parse(tb1.Text);
            miles2 = km * 1.609344;
            text2.Text = ($"{miles2}{k}");

        }

        private void button_Click(object sender, RoutedEventArgs e)
        {
            km = int.Parse(tb1.Text);
            km2 = km * 0.62137119;
            text2.Text = ($"{km2}{m}");

        }

        public MainPage()
        {
            this.InitializeComponent();

            text1.Text = q7;
        }
    }
}
