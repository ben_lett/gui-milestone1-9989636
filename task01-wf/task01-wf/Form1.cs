﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace task01_wf
{
    public partial class Form1 : Form
    {
        public static string q1 = "What would you like to convert to?";
        public static string q2 = "\n1. KMs to Miles";
        public static string q3 = "\n2. Miles to Kms ";
        public static string q4 = "\n3. exit the program";
        public static string q5 = "Enter the ammount of kms you want to conver to miles";
        public static string q6 = "Enter the ammount of Miles you want to conver to KM's";
        public static string q7 = "Enter the ammount of miles or kms you want to convert";
        public static string enter = "press<enter> to continue";
        public static string thanks = "Thanks for using";
        public static string please = "Please choose a number listed.";
        public static string convert = "your converted distance is";
        public static string k = "kms";
        public static string m = "miles";      
        double km, km2, miles2;

        public Form1()
        {
            
           

            InitializeComponent();

            label1.Text = (q7);
           
        }

        private void button1_Click(object sender, EventArgs e)
        {
            km = int.Parse(tb1.Text);
            km2 = km * 0.62137119;
            label2.Text = ($"{km2}");
            label3.Text = (m);
        }

        private void button2_Click(object sender, EventArgs e)
        {
            km = int.Parse(tb1.Text);
            miles2 = km * 1.609344;
            label2.Text = ($"{miles2}");
            label3.Text = (k);
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }
    }
}
