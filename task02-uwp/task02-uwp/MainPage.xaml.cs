﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=402352&clcid=0x409

namespace task02_uwp
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class MainPage : Page
    {

        static string q1 = "Enter the prices of grocery then click the add item button";
        static string q2 = "To finish adding items click the find gst button";
        static string q3 = "Your total is $";
        static string q4 = "Your total including GST is $";

        static double total = 0.00;
        static double total1 = 0.00;
        static double gst = 0.00;

        public MainPage()
        {
            this.InitializeComponent();
            text1.Text = q1;
            text2.Text = q2;
        
        }

        private void b1_Click(object sender, RoutedEventArgs e)
        {
            total = double.Parse(tb1.Text);
            total1 = total1 + total;
            tb1.Text = String.Empty;

        }

        private void b2_Click(object sender, RoutedEventArgs e)
        {
            gst = total * 1.15;
            total = total + 0;
            total = System.Math.Round(total, 2);
            total1 = System.Math.Round(total1, 2);
            total.ToString("#0.00");
            total1.ToString("#0.00");
            text3.Text = (q3 + total1);
            text4.Text = (q4 + gst);
        }
    }
}
