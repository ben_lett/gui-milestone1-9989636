﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace task02_wf
{
    public partial class Form1 : Form
    {
        static string q1 = "Enter the prices of grocery then click the add item button";
        static string q2 = "To finish adding items click the find gst button";
        static string q3 = "Your total is $";
        static string q4 = "Your total including GST is $";

        static double total = 0.00;
        static double total1 = 0.00;
        static double gst = 0.00;

        public Form1()
        {
            InitializeComponent();
            label1.Text = (q1);
            label2.Text = (q2);
        }

        private void button1_Click(object sender, EventArgs e)
        {
            total= double.Parse(textBox1.Text);
            total1 = total1 + total;
            textBox1.Text = String.Empty;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            gst = total * 1.15;
            total = total + 0;
            total = System.Math.Round(total, 2);
            total1 = System.Math.Round(total1, 2);
            total.ToString("#0.00");
            total1.ToString("#0.00");
            label3.Text = (q3 + total1);
            label4.Text = (q4 + gst);
        }
    }
}
