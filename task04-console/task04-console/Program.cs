﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace task_04_console
{
    class Program
    {
        static string q1 = "Welcome to the produce section";
        static string q2 = "Type the name of a vegetble or fruit to see if we have it in stock.";
        static string q3 = "press <enter> key to search for another item.";
        static string q4 = "to exit program press 0 followed by <enter> key ";

        static void Main(string[] args)
        {
            while (true)
            {
                var check = "";
                var choose = "";

                Console.Clear();

                Console.WriteLine(q1);
                Console.WriteLine(q2);
                Console.WriteLine(q4);
                check = Console.ReadLine();
                if (check == "0")
                    {
                    Console.Clear();
                    Console.WriteLine("Thanks for using my program.");
                    Console.ReadKey();
                    break;
                }

                else
                {
                    Console.Clear();

                    Console.WriteLine(checkproduce(check));

                    Console.WriteLine(q3);
                    Console.WriteLine(q4);
                    choose = Console.ReadLine();

                    if (choose == "0")
                    {
                        Console.Clear();
                        Console.WriteLine("Thanks for using my program.");
                        Console.ReadKey();
                        break;
                    }
                    else
                    {

                    }
                }
            }

        }

        static string checkproduce(string check)
        {
            var output = "";
            var produce = new Dictionary<string, string>();

            produce.Add("orange", "fruit");          
            produce.Add("mango", "fruit");
            produce.Add("mangos", "fruit");
            produce.Add("kiwifruit", "fruit");
            produce.Add("banana", "fruit");
            produce.Add("bananas", "fruit");
            produce.Add("pear", "fruit");
            produce.Add("pears", "fruit");
            produce.Add("dates", "fruit");
            produce.Add("apple", "fruit");
            produce.Add("peach", "fruit");
            produce.Add("grapes", "fruit");
            produce.Add("leamon", "fruit");
            produce.Add("lime", "fruit");
            produce.Add("pineapple", "fruit");


            produce.Add("cucumber", "vegetable");
            produce.Add("lettuce", "vegetable");
            produce.Add("tomatos", "vegetable");
            produce.Add("egg plant", "vegetable");
            produce.Add("leeks", "vegetable");
            produce.Add("celery", "vegetable");
            produce.Add("carrot", "vegetable");
            produce.Add("carrots", "vegetable");
            produce.Add("potato", "vegetable");
            produce.Add("potatos", "vegetable");
            produce.Add("potatoes", "vegetable");
            produce.Add("cabbage", "vegetable");
            produce.Add("cabbages", "vegetable");
            produce.Add("yam", "vegetable");
            produce.Add("kumara", "vegetable");

            if (produce.ContainsKey(check))
            {
                output = $"You lucky, Jal'yt we have {check} in stock!";
            }

            else
            {
                output = $"You not lucky,We dont currently have {check} today. Maybe next time, Jal'Yt!";
            }

            return output;
        }
    }
}