﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace task04_wf
{
    public partial class Form1 : Form
    {
        static string q1 = "Welcome to the produce section";
        static string q2 = "Type the name of a vegetble or fruit.";
        static string q3 = "click the check button to see if we have it in stock";

        public Form1()
        {
            InitializeComponent();
            label1.Text = (q1);
            label2.Text = (q2);
            label3.Text = (q3);

        }

        private void button1_Click(object sender, EventArgs e)
        {
            var check = "";
            check = (textBox1.Text);
            label4.Text = (checkproduce(check));
            textBox1.Text = String.Empty;

        }




        static string checkproduce(string check)
        {
            var output = "";
            var produce = new Dictionary<string, string>();

            produce.Add("orange", "fruit");
            produce.Add("mango", "fruit");
            produce.Add("mangos", "fruit");
            produce.Add("kiwifruit", "fruit");
            produce.Add("banana", "fruit");
            produce.Add("bananas", "fruit");
            produce.Add("pear", "fruit");
            produce.Add("pears", "fruit");
            produce.Add("dates", "fruit");
            produce.Add("apple", "fruit");
            produce.Add("peach", "fruit");
            produce.Add("grapes", "fruit");
            produce.Add("leamon", "fruit");
            produce.Add("lime", "fruit");
            produce.Add("pineapple", "fruit");


            produce.Add("cucumber", "vegetable");
            produce.Add("lettuce", "vegetable");
            produce.Add("tomatos", "vegetable");
            produce.Add("egg plant", "vegetable");
            produce.Add("leeks", "vegetable");
            produce.Add("celery", "vegetable");
            produce.Add("carrot", "vegetable");
            produce.Add("carrots", "vegetable");
            produce.Add("potato", "vegetable");
            produce.Add("potatos", "vegetable");
            produce.Add("potatoes", "vegetable");
            produce.Add("cabbage", "vegetable");
            produce.Add("cabbages", "vegetable");
            produce.Add("yam", "vegetable");
            produce.Add("kumara", "vegetable");

            if (produce.ContainsKey(check))
            {
                output = $"You lucky, Jal'yt we have {check} in stock!";
            }

            else
            {
                output = $"You not lucky,We dont currently have {check} today. Maybe next time, Jal'Yt!";
            }

            return output;
        }
    }
}
