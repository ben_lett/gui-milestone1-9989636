﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=402352&clcid=0x409

namespace task05_uwp
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class MainPage : Page
    {
        static int points = 0;
        static int random = 0;
        static int turns = 0;
        static int turns2 = 5;


        static string q1 = "click your guess";
        static string q2 = "Im thinking of a number between 1 and 5";
        static string q3 = "you get 5 turns, how well will you do?";
        static string q4 = "play again? click reset to reset your turns";
        static string q5 = "click your next guess";

        public MainPage()
        {
            this.InitializeComponent();
            t1.Text = q2;
            t2.Text = q3;
            t3.Text = q1;
        }

        private void button_Click(object sender, RoutedEventArgs e)
        {
            var check = 1;
            t5.Text = (correctornot(check));
            turns++;
            turns2--;

            if (turns == 5)
            {
                t4.Text = ($"You scored {points} out of 5");
                t6.Text = (q4);
            }
            else if (turns > 5)
            {
                t5.Text = (q4);
                t6.Text = "";
            }
            else
            {
                t5.Text = (correctornot(check));
                t6.Text = (q5);
            }

        }

        private void button1_Click(object sender, RoutedEventArgs e)
        {
            var check = 2;
            t5.Text = (correctornot(check));
            turns++;
            turns2--;

            if (turns == 5)
            {
                t4.Text = ($"You scored {points} out of 5");
                t6.Text = (q4);
            }
            else if (turns > 5)
            {
                t5.Text = (q4);
                t6.Text = "";
            }
            else
            {
                t5.Text = (correctornot(check));
                t6.Text = (q5);
            }
        }

        private void button2_Click(object sender, RoutedEventArgs e)
        {
            var check = 3;
            t5.Text = (correctornot(check));
            turns++;
            turns2--;

            if (turns == 5)
            {
                t4.Text = ($"You scored {points} out of 5");
                t6.Text = (q4);
            }
            else if (turns > 5)
            {
                t5.Text = (q4);
                t6.Text = "";
            }
            else
            {
                t5.Text = (correctornot(check));
                t6.Text = (q5);
            }
        }

        private void button3_Click(object sender, RoutedEventArgs e)
        {
            var check = 4;
            t5.Text = (correctornot(check));
            turns++;
            turns2--;

            if (turns == 5)
            {
                t4.Text = ($"You scored {points} out of 5");
                t6.Text = (q4);
            }
            else if (turns > 5)
            {
                t5.Text = (q4);
                t6.Text = "";
            }
            else
            {
                t5.Text = (correctornot(check));
                t6.Text = (q5);
            }
        }

        private void button4_Click(object sender, RoutedEventArgs e)
        {
            var check = 5;
            t5.Text = (correctornot(check));
            turns++;
            turns2--;

            if (turns == 5)
            {
                t4.Text = ($"You scored {points} out of 5");
                t6.Text = (q4);
            }
            else if (turns > 5)
            {
                t5.Text = (q4);
                t6.Text = "";
            }
            else
            {
                t5.Text = (correctornot(check));
                t6.Text = (q5);
            }
        }

        private void button5_Click(object sender, RoutedEventArgs e)
        {
            t5.Text = "";
            t4.Text = "";
            t6.Text = "";
            turns = 0;
            turns2 = 4;
            points = 0;

        }
        static string correctornot(int check)
        {
            var output = "";
            var rand = randomnum(random);

            if (randomnum(random) == check)
            {
                points = points + 1;
                output = $"you guessed {check} i was thinking of {randomnum(random)} well done!\nyou have {turns2} turns left.";
            }
            else
            {
                output = $"you guessed {check} i was thinking of {randomnum(random)}\nyou have {turns2} turns left.";
            }

            return output;
        }
        static int randomnum(int random)
        {
            Random rnd = new Random();
            int answer = rnd.Next(1, 6);

            return answer;
        }
    }
}
