﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace task05_wf
{
    public partial class Form1 : Form
    {
        static int points = 0;
        static int random = 0;
        static int turns = 0;
        static int turns2 = 5;
        

        static string q1 = "click your guess";
        static string q2 = "Im thinking of a number between 1 and 5";
        static string q3 = "you get 5 turns, how well will you do?";
        static string q4 = "play again? click reset to reset your turns";
        static string q5 = "click your next guess";
        public Form1()
        {
            InitializeComponent();

            label1.Text = (q2);
            label2.Text = (q3);
            label3.Text = (q1);

        }
    

        private void button1_Click(object sender, EventArgs e)
        {
            var check = 1;
            label5.Text = (correctornot(check));
            turns++;
            turns2--;

            if (turns == 5)
            {
                label4.Text = ($"You scored {points} out of 5");
                label6.Text = (q4);
            }
            else if (turns > 5)
            {
                label5.Text = (q4);
                label6.Text = "";
            }
            else
            {
                label5.Text = (correctornot(check));
                label6.Text = (q5);
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            var check = 2;
            label5.Text = (correctornot(check));
            turns++;
            turns2--;

            if (turns == 5)
            {
                label4.Text = ($"You scored {points} out of 5");
                label6.Text = (q4);
            }
            else if (turns > 5)
            {
                label5.Text = (q4);
                label6.Text = "";
            }
            else
            {
                label5.Text = (correctornot(check));
                label6.Text = (q5);
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            var check = 3;
            
            turns++;
            turns2--;

            if (turns == 5)
            {
                label4.Text = ($"You scored {points} out of 5");
                label6.Text = (q4);
            }
            else if(turns >5)
            {
                label5.Text = (q4);
                label6.Text = "";
            }
            else
            {
                label5.Text = (correctornot(check));
                label6.Text = (q5);
            }
        }

        private void button4_Click(object sender, EventArgs e)
        {
            var check = 4;
            label5.Text = (correctornot(check));
            turns++;
            turns2--;

            if (turns == 5)
            {
                label4.Text = ($"You scored {points} out of 5");
                label6.Text = (q4);
            }
            else if (turns > 5)
            {
                label5.Text = (q4);
                label6.Text = "";
            }
            else
            {
                label5.Text = (correctornot(check));
                label6.Text = (q5);
            }
        }

        private void button5_Click(object sender, EventArgs e)
        {
            var check = 5;
            label5.Text = (correctornot(check));
            turns++;
            turns2--;

            if (turns == 5)
            {
                label4.Text = ($"You scored {points} out of 5");
                label6.Text = (q4);
            }
            else if (turns > 5)
            {
                label5.Text = (q4);
                label6.Text = "";
            }
            else
            {
                label5.Text = (correctornot(check));
                label6.Text = (q5);
            }
        }

        static int randomnum(int random)
        {
            Random rnd = new Random();
            int answer = rnd.Next(1, 6);

            return answer;
        }

        static string correctornot(int check)
        {
            var output = "";
            var rand = randomnum(random);

            if (randomnum(random) == check)
            {
                points = points + 1;
                output = $"you guessed {check} i was thinking of {randomnum(random)} well done!\nyou have {turns2} turns left.";
            }
            else
            {
                output = $"you guessed {check} i was thinking of {randomnum(random)}\nyou have {turns2} turns left.";
            }

            return output;
        }

        private void button6_Click(object sender, EventArgs e)
        {
            label5.Text = "";
            label4.Text = "";
            label6.Text = "";
            turns = 0;
            turns2 = 4;
            points = 0;
        }
    }
}
