﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=402352&clcid=0x409

namespace tesk03_uwp
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class MainPage : Page
    {
        static string q1 = "Press a button for a list of names";
        static string f1 = "apple";
        static string f2 = "peach";
        static string f3 = "pear";
        static string f4 = "grapes";
        static string c1 = "blue";
        static string c2 = "pink";
        static string c3 = "orange";
        static string c4 = "yellow";
        static string n1 = "mr mittens";
        static string n2 = "wiskers";
        static string n3 = "Captian meow";
        static string n4 = "frank";
        public MainPage()
        {
            this.InitializeComponent();
            text1.Text = q1;
        }

        private void button_Click(object sender, RoutedEventArgs e)
        {
            text2.Text = f1;
            text3.Text = f2;
            text4.Text = f3;
            text5.Text = f4;
        }

        private void button1_Click(object sender, RoutedEventArgs e)
        {
            text2.Text = n1;
            text3.Text = n2;
            text4.Text = n3;
            text5.Text = n4;
        }

        private void button2_Click(object sender, RoutedEventArgs e)
        {
            text2.Text = c1;
            text3.Text = c2;
            text4.Text = c3;
            text5.Text = c4;
        }
    }
}
