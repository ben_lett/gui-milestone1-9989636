﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=402352&clcid=0x409

namespace tesk04_uwp
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class MainPage : Page
    {
        static string q1 = "Welcome to the produce section";
        static string q2 = "Type the name of a vegetble or fruit.";
        static string q3 = "click the check button to see if we have it in stock";
        public MainPage()
        {
            this.InitializeComponent();
            text1.Text = q1;
            text2.Text = q2;
            text3.Text = q3;
        }


        static string checkproduce(string check)
        {
            var output = "";
            var produce = new Dictionary<string, string>();

            produce.Add("orange", "fruit");
            produce.Add("mango", "fruit");
            produce.Add("mangos", "fruit");
            produce.Add("kiwifruit", "fruit");
            produce.Add("banana", "fruit");
            produce.Add("bananas", "fruit");
            produce.Add("pear", "fruit");
            produce.Add("pears", "fruit");
            produce.Add("dates", "fruit");
            produce.Add("apple", "fruit");
            produce.Add("peach", "fruit");
            produce.Add("grapes", "fruit");
            produce.Add("leamon", "fruit");
            produce.Add("lime", "fruit");
            produce.Add("pineapple", "fruit");


            produce.Add("cucumber", "vegetable");
            produce.Add("lettuce", "vegetable");
            produce.Add("tomatos", "vegetable");
            produce.Add("egg plant", "vegetable");
            produce.Add("leeks", "vegetable");
            produce.Add("celery", "vegetable");
            produce.Add("carrot", "vegetable");
            produce.Add("carrots", "vegetable");
            produce.Add("potato", "vegetable");
            produce.Add("potatos", "vegetable");
            produce.Add("potatoes", "vegetable");
            produce.Add("cabbage", "vegetable");
            produce.Add("cabbages", "vegetable");
            produce.Add("yam", "vegetable");
            produce.Add("kumara", "vegetable");

            if (produce.ContainsKey(check))
            {
                output = $"You lucky, Jal'yt we have {check} in stock!";
            }

            else
            {
                output = $"You not lucky,We dont currently have {check} today. Maybe next time, Jal'Yt!";
            }

            return output;
        }

        private void button_Click(object sender, RoutedEventArgs e)
        {
            var check = "";
            check = (tb1.Text);
            text4.Text = (checkproduce(check));
            tb1.Text = String.Empty;
        }
    }

}
